const gulp = require('gulp');
const cleanDir = require('gulp-clean');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const distDir = './dist';

gulp.task('clean', function (callback) {
  return gulp.src(distDir, { read: false, allowEmpty: true }).pipe(cleanDir());
});

gulp.task('build:lib', function (callback) {
  return gulp
    .src('lib/*.js')
    .pipe(
      babel({
        presets: ['@babel/env'],
      })
    )
    .pipe(uglify())
    .pipe(gulp.dest(`${distDir}/lib`));
});
gulp.task('build:bin', function (callback) {
  return gulp
    .src('bin/*.js')
    .pipe(
      babel({
        presets: ['@babel/env'],
      })
    )
    .pipe(uglify())
    .pipe(gulp.dest(`${distDir}/bin`));
});
gulp.task('copy', function (callback) {
  return gulp.src(['./README.md']).pipe(gulp.dest(distDir));
});
gulp.task('copyJson', function (callback) {
  return gulp
    .src(['./package.prod.json'])
    .pipe(rename('package.json'))
    .pipe(gulp.dest(distDir));
});
gulp.task('copyImg', function (callback) {
  return gulp.src(['./img/**']).pipe(gulp.dest(`${distDir}/img`));
});
exports.default = gulp.series([
  'clean',
  'build:lib',
  'build:bin',
  'copy',
  'copyJson',
]);
