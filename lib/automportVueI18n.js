//判断是否已经引入了 i18n， 若没有引入，则在文件头部引入
const autoImportVue2I18n = (content) => {
  // 判断是否有中文，没有中文直接忽略
  if (!/[\u4e00-\u9fa5]+/gim.test(content)) return content;
  //判断是否已经引入了 Vue， 若没有引入，则在文件头部引入
  let vueMatch = content.match(
    /(import[\s\t]+([^\s\t]+)[\s\t]+from[\s\t]+('vue'|"vue")[\s\t]*;?)|((let|var|const)[\s\t]+([^\s\t]+)[\s\t]+\=[\s\t]+require\(('vue'|"vue")\)[\s\t]*;?)/m
  );
  let vueModule = 'Vue';
  if (!vueMatch) {
    content = `//I18N auto import START\nimport Vue from 'vue';\n//I18N auto import END\n${content}`;
  } else {
    vueModule = vueMatch[2] || vueMatch[5];
  }
  let imports = content.match(/from[\s\t]+['"][^'"]+['"][\s\t]*;?/gm);
  let lastImport = imports[imports.length - 1];
  //判断是否已经做过绑定 $t 的绑定，若没有，则自动绑定 $t
  if (
    !content.match(
      /const[\s\t]+\$t[\s\t]+=[\s\t]+_i18n_vue.\$t.bind\(_i18n_vue\)[\s\t]*;?/
    )
  ) {
    const isI18n = lastImport.includes('vue');
    if (isI18n) {
      content = content.replace(/\/\/\s*I18N auto import END(\n|\r)/, '');
      content = content.replace(lastImport, ($) => {
        return `${$}\n//I18N auto import END\n//I18N auto bind START\nlet _i18n_vue = new ${vueModule}();\nconst $t = _i18n_vue.$t.bind(_i18n_vue);\n//I18N auto bind END\n`;
      });
    } else {
      content = content.replace(lastImport, ($) => {
        return `${$}\n//I18N auto bind START\nlet _i18n_vue = new ${vueModule}();\nconst $t = _i18n_vue.$t.bind(_i18n_vue);\n//I18N auto bind END\n`;
      });
    }
  }
  return content;
};

const autoImportVue3I18n = (content, isJS) => {
  if (!/[\u4e00-\u9fa5]+/gim.test(content)) return content;
  //判断是否已经引入了 vue-i18n， 若没有引入，则在文件头部引入
  let vueMatch = content.match(
    /(import[\s\t]+{\s*useI18n\s*}[\s\t]+from[\s\t]+('vue-i18n'|"vue-i18n")[\s\t]*;?)|((let|var|const)[\s\t]+{\s*useI18n\s*}[\s\t]+\=[\s\t]+require\(('vue-i18n'|"vue-i18n")\)[\s\t]*;?)/m
  );
  let vueModule = 'vue-i18n';
  if (!vueMatch) {
    const importI18n = isJS
      ? `import i18n from '@/i18n'`
      : `import { useI18n } from '${vueModule}'`;
    content = `//I18N auto import START\n${importI18n};\n//I18N auto import END\n${content}`;
  }
  let imports = content.match(/from[\s\t]+['"][^'"]+['"][\s\t]*;?/gm);
  let lastImport = imports[imports.length - 1];
  //判断是否已经做过绑定 $t 的绑定，若没有，则自动绑定 $t
  if (
    !content.match(
      /const[\s\t]+\{\s*t\s*=\s*\$t\s*}[\s\t]+=[\s\t]+useI18n\(\)[\s\t]*;?/
    )
  ) {
    const isI18n = lastImport.includes('vue-i18n');
    const useI18n = isJS
      ? `const { t: $t } = i18n.global`
      : `const { t: $t } = useI18n()`;
    if (isI18n) {
      content = content.replace(/\/\/\s*I18N auto import END(\n|\r)/, '');
      content = content.replace(lastImport, ($) => {
        return `${$}\n//I18N auto import END\n//I18N auto bind START\n${useI18n};\n//I18N auto bind END\n`;
      });
    } else {
      content = content.replace(lastImport, ($) => {
        return `${$}\n//I18N auto bind START\n${useI18n};\n//I18N auto bind END\n`;
      });
    }
  }
  return content;
};

module.exports = {
  autoImportVue2I18n,
  autoImportVue3I18n,
};
