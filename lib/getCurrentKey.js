const path = require('path');
const _ = require('lodash');
const { Md5_16 } = require('./utils');
/**
 * 获取key前缀
 * @param file
 * @returns {string}
 */
const getPreKey = (rootPath, file, config) => {
  let filePath = path.relative(rootPath, file);
  const extname = path.extname(filePath);
  filePath = filePath.replace(extname, '');
  if (filePath.includes('/')) {
    filePath = filePath
      .split('/')
      .map((el) => _.camelCase(el))
      .join('.');
  } else {
    filePath = filePath
      .split('\\')
      .map((el) => _.camelCase(el))
      .join('.');
  }
  filePath = filePath + '.';
  return config.key ? `${_.camelCase(config.key)}.` + filePath : filePath;
};

/**
 * 获取当前key
 * @returns {*}
 */
const getCurrentKey = (
  match,
  file,
  messages,
  messagesHash,
  rootPath,
  config
) => {
  match = match.replace(/['"`]/g, '').trim();
  if (messagesHash[match]) return messagesHash[match];
  // let key = getPreKey(file) + generate++
  let key = getPreKey(rootPath, file, config);
  if (!messages[key]) {
    // return key.toLowerCase()
    // let camelCaseKey = await translateText(match, baiduAppid, baiduKey);
    let camelCaseKey = _.camelCase(Md5_16(match));
    const first = camelCaseKey.slice(0, 1).toUpperCase();
    camelCaseKey = camelCaseKey.slice(1);
    return key + first + camelCaseKey;
  }
  return getCurrentKey(match, file);
};

module.exports = getCurrentKey;
