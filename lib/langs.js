module.exports = [
  // {
  //   value: "zh-CN",
  //   label: "简体中文",
  //   checked: true,
  // },
  {
    value: "en-US",
    label: "英国 - 美国",
    checked: true,
  },
  {
    value: "zh-TW",
    label: "繁体中文(台湾地区)",
  },
  {
    value: "zh-HK",
    label: "繁体中文(香港)",
  },
  {
    value: "fr-FR",
    label: "法国",
  },
  {
    value: "de-DE",
    label: "德国",
  },
  {
    value: "it-IT",
    label: "意大利",
  },
  {
    value: "ja-JP",
    label: "日本",
  },
  {
    value: "ko-KR",
    label: "韩国",
  },
  {
    value: "ru-RU",
    label: "俄国",
  },
];
