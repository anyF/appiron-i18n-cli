/*
 * @Author: 段本显
 * @FilePath: /appiron-i18n-cli/lib/replaceScript.js
 * @Date: 2022-08-08 17:53:49
 * @LastEditors: 段本显
 * @LastEditTime: 2022-11-25 11:28:31
 * @Email: duanbenxian@appiron.cn
 * 如果有bug，那肯定不是我的锅，噜噜噜
 */
const replaceFucn = require('./replaceFucn');
class ReplaceJsContent extends replaceFucn {
  constructor(props) {
    super(props);
    this.replace();
  }

  // 替换中文
  replaceContent() {
    const matchReg =
      /(['"`])(((?!\1)[\s\S])*[\u4e00-\u9fa5]+((?!\1)[\s\S])*)\1[^\$]/gim;
    this.content = this.content.replace(matchReg, (text) => {
      return this.replaceChinese(text, this.suffix ?? '');
    });
    return this;
  }

  replace() {
    this.replaceContent();
  }
}

module.exports = ReplaceJsContent;
