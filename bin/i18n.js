#!/usr/bin/env node
const colors = require('colors');
const path = require('path');
// 交互式命令行
const inquirer = require('inquirer');
const packageInfo = require('../package.json');
const program = require('commander');
const generateFile = require('../lib/generate');
const revertFile = require('../lib/revert');
const langList = require('../lib/langs');
const packageJson = require(path.join(process.cwd(), '/package.json')) || {};

const generate = async (
  src = 'src',
  { key, path, single, filename = 'zh-CN', langs, baiduAppid, baiduKey }
) => {
  return generateFile.generate(src, {
    key,
    single,
    path,
    filename,
    langs,
    baiduAppid,
    baiduKey,
  });
};

const revert = (src = 'src', { path, filename = 'zh-CN' }) => {
  return revertFile.revert(src, { path, filename });
};
program.version(packageInfo.version, '-v, --version');

program
  .command('create')
  .description('自动提取国际化文案')
  .action(async () => {
    let options = [
      {
        name: 'type',
        type: 'list',
        message: '请选择操作类型：',
        choices: ['翻译文案', '取消国际化'],
        filter(val) {
          const valueList = {
            翻译文案: 'generate',
            取消国际化: 'revert',
          };
          // 使用filter将回答变为小写
          return valueList[val];
        },
      },
    ];
    // 选择国际化还是还原国际化
    const answers = await inquirer.prompt(options);
    // answers 就是用户输入的内容，是个对象
    let { type, url } = answers;
    if (type === 'generate') {
      //选择国际化
      const dirPrompList = [
        {
          type: 'input',
          message: '请输入执行国际化的目录,默认为src目录：',
          name: 'src',
          default: 'src',
        },
        {
          type: 'checkbox',
          message: '请选择需要的国际化语言：',
          name: 'langs',
          choices: langList.map((lang) => ({
            name: `${lang.value} [  ${lang.label}]`,
            checked: !!lang.checked,
          })),
        },
        {
          type: 'input',
          message: '请输入百度翻译的Appid',
          name: 'baiduAppid',
          validate(value) {
            if (value) return true;
            else return '请输入百度翻译的Appid';
          },
          when(answers) {
            // 当key 不存在才提示
            return answers.langs?.length;
          },
        },
        {
          type: 'input',
          message: '请输入百度翻译的key',
          name: 'baiduKey',
          validate(value) {
            if (value) return true;
            else return '请输入百度翻译的key';
          },
          when(answers) {
            // 当key 不存在才提示
            return answers.langs?.length;
          },
        },
        {
          type: 'input',
          message: '请输入定义key前缀，默认为相对执行目录的文件路径：',
          name: 'key',
          default: packageJson.name || '',
        },
        {
          type: 'confirm',
          message: '是否为单文件index序列，默认为全局序列？',
          name: 'single',
          when(answers) {
            // 当key 不存在才提示
            return !answers.key;
          },
          default: false,
        },
        {
          type: 'input',
          message: '是否设置国际化生成文件路径，默认为运行目录下i18n？',
          name: 'path',
          default: 'i18n',
        },
      ];
      const dirAnswer = await inquirer.prompt(dirPrompList);
      const {
        src = 'src',
        path,
        key,
        single,
        langs,
        baiduAppid,
        baiduKey,
      } = dirAnswer;
      await generate(src, { path, key, single, langs, baiduAppid, baiduKey });
      console.log(`${colors.green('✔')} ${colors.green('Success')}`);
    } else {
      //还原国际化
      const dirPrompList = [
        {
          type: 'input',
          message: '请输入国际化还原目录,默认src目录：',
          name: 'src',
          default: 'src',
        },
        {
          type: 'input',
          message: '读取国际化文件路径，默认为运行目录下i18n？',
          name: 'path',
          default: 'i18n',
        },
      ];
      const dirAnswer = await inquirer.prompt(dirPrompList);
      const { src = 'src', path } = dirAnswer;
      revert(src, { path });
      console.log(`${colors.green('✔')} ${colors.green('Success')}`);
    }
  });

program.on('command:*', function () {
  console.error(
    'Invalid command: %s\nSee --help for a list of available commands.',
    program.args.join(' ')
  );
  process.exit(1);
});
if (process.argv.length === 2) {
  program.help();
}
program.parse(process.argv);
