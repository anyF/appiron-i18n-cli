# appiron-i18n 替换工具

## 一键自动提取和恢复vue项目国际化，解决国际化复杂的痛点，目前工具还有隐藏未知的问题，希望大家踊跃提issue，也希望大家一起来维护。



### 安装
```
sudo npm install -g appiron-i18n-cli
sudo yarn global add appiron-i18n-cli
```
### 自动国际化

#### 1.在项目根目录执行命令

```
i18n create
```
![选择翻译文案](https://cdn.staticaly.com/gh/17572622/images@master/appiron-i18n-cli/amy1.png)
#### 2.输入需要提取文案的目录，默认为src目录
![选择翻译目录](https://cdn.staticaly.com/gh/17572622/images@master/appiron-i18n-cli/amy2.png)
#### 3.选择需要翻译的语言，目前支持的语言有以下几种（翻译使用的是百度翻译，需要输入百度翻译开发者appid和key）
![选择翻译语言](https://cdn.staticaly.com/gh/17572622/images@master/appiron-i18n-cli/amy3.png)
#### 4.输入appid和key(第三步如果没有选择需要翻译的语言，会直接跳过这一步)
![输入百度翻译id和key](https://cdn.staticaly.com/gh/17572622/images@master/appiron-i18n-cli/amy12.png)
#### 5.可以自己输入文案前缀，默认为项目名称
![选择自定义前缀](https://cdn.staticaly.com/gh/17572622/images@master/appiron-i18n-cli/amy4.png)
#### 6.如果输入了自定义前缀就不能设置为单文件，单文件序列就是生成文案的key就是翻译的文件名
![是否是单文件](https://cdn.staticaly.com/gh/17572622/images@master/appiron-i18n-cli/amy5.png)
#### 7.设置文案生成目录，默认为根目录下的i18n目录，没有会自动生成
![设置文案生成目录](https://cdn.staticaly.com/gh/17572622/images@master/appiron-i18n-cli/amy6.png)
#### 8.全部选择完成按回车键进行自动文案提取和翻译,最后出现Success
![success](https://cdn.staticaly.com/gh/17572622/images@master/appiron-i18n-cli/amy7.png)

### 自动恢复国际化
#### 1.在项目根目录执行命令

```
i18n create
```
![选择还原国际化](https://cdn.staticaly.com/gh/17572622/images@master/appiron-i18n-cli/amy8.png)
#### 1.输入需要还原的目录，默认为src目录
![选择翻译目录](https://cdn.staticaly.com/gh/17572622/images@master/appiron-i18n-cli/amy9.png)
#### 2.选择还原文案所在目录,默认为根目录下的i18n目录下的zh-CH.json文件
![选择翻译语言](https://cdn.staticaly.com/gh/17572622/images@master/appiron-i18n-cli/amy10.png)
#### 3.最后回车键确认还原国际化，出现Success表示成功
![是否是单文件](https://cdn.staticaly.com/gh/17572622/images@master/appiron-i18n-cli/amy11.png)

### 注意事项
#### 1. vue模板中不能模板字符串和中文混合使用，只能使用其中任意一种
![amy01](https://cdn.staticaly.com/gh/17572622/images@master/appiron-i18n-cli/amy01.png)

#### 2. vue模板中文不能出现<>这种符号，如果确实不能避免可以使用\&gt; \&lt;代替
![amy02](https://cdn.staticaly.com/gh/17572622/images@master/appiron-i18n-cli/amy02.png)


### 参考
[vue-i18n 文档](https://kazupon.github.io/vue-i18n/)
[vue-i18n-generator 文档](https://gitee.com/thesadboy/vue-i18n-generator/)
